package dependencies;

public class Order {
    private String product;

    public Order (String product) {
        this.product = product;
    }

    public boolean isValid() {
        return !product.isEmpty();
    }

}
