import dependencies.DBRepository;
import dependencies.MailSender;
import dependencies.Order;

public class OrderProcessor {
    public void process(Order order) throws Exception {
        if (order.isValid() && new DBRepository().save(order)) {
            new MailSender().sendConfirmationMessage(order);
        }
    }
}